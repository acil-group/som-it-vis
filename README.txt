﻿The code provided here to generate the IT-vis makes use of the SOMToolbox© for MATLAB

J. Vesanto, J. Himberg, E. Alhoniemi, and J. Parhankangas, “Self-Organizing Map in Matlab: the SOM Toolbox,” in Proceedings of the
Matlab DSP Conference, 1999, pp. 35–40.

which is available at:

http://www.cis.hut.fi/projects/somtoolbox

If using SOMToolbox©, please abide by its copyright rules and reference it appropriately.

The data sets used in the experiments section of the IT-vis article are available at:

UCI machine learning repository: http://archive.ics.uci.edu/ml
Fundamental Clustering Problems Suite (FCPS): http://www.uni-marburg.de/fb12/datenbionik/data?language_sync=1
Clustering data sets - shape sets: http://cs.joensuu.fi/sipu/datasets

The "main_example.m" file contains an example of usage of the IT-vis code.